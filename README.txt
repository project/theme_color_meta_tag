INTRODUCTION
------------
Adds a "theme-color" meta tag and administration UI on appearance section to
set color value. This tag gives ability for your web-site to look in its own
color theme in mobile Chrome and Safari browser. Other modules may also change
color value using hook_theme_color_meta_tag_color_alter().

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/2514202

REQUIREMENTS
------------
This module requires the following modules:
 * Color (Core module)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
 * Configure user permissions in Administration » Appearance » Settings:

   - Choose color value for "theme-color" meta tag.

MAINTAINERS
-----------
Current maintainers:
 * Vitaliy Bobrov (bobrov1989) - https://drupal.org/user/2821167

This project has been sponsored by:
 * FFW
