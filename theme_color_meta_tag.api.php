<?php

/**
 * @file
 * Documentation for Theme-color meta tag API.
 */

/**
 * Alter the color value for theme-color.
 *
 * @param string $color
 *   A hexadecimal CSS color string.
 *
 * @see theme_color_meta_tag_preprocess_html()
 */
function hook_theme_color_meta_tag_color_alter(&$color) {
  // Change theme-color meta tag to custom value.
  $color = '#AEAEAE';
}
