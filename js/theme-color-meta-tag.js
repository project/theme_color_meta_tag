/**
 * @file
 * Attaches the behaviors for the Theme-color meta tag module.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.themeColorColorpicker = {
    attach: function (context, settings) {
      // Prevent multiple processing.
      $('#theme-color-colorpicker', context).once('theme-color-colorpicker-processed', function () {
        // Create a farbtastic colorpicker in the given DIV container.
        var colorPicker = $.farbtastic('#theme-color-colorpicker-container');

        // Retrieve the target text field that contains the HEX color strings.
        var colorPickerTarget = $('#theme-color-colorpicker');

        // Make the color picker auto-update the contents of the HEX text field.
        colorPicker.linkTo(colorPickerTarget);
      });
    }
  };
})(jQuery);
